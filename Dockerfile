FROM ubuntu:22.04

RUN set -eu; apt-get update; DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y git hdparm sg3-utils nvme-cli rst2pdf fio python3-matplotlib python3-lxml python3-numpy python3-simplejson python3-setuptools ca-certificates tini lsb-release; apt-get clean; rm -rf /var/cache/apt/lists
RUN set -eu; cd /opt; git clone https://github.com/thomas-krenn/TKperf.git
RUN set -eu; cd /opt/TKperf; python3 setup.py install
